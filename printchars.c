#include "csapp.h"
#include "sbuf.h"
#include "string.h"

#define TIEMPO_MS 50
#define BUFFER_SIZE 10

sbuf_t buffer;
void *escribir(void * argv);
void *leer(void *argv);


int readcnt;
sem_t mutex, w;

int main(int argc, char **argv)
{
	char *filename;
	struct stat fileStat;
	pthread_t consumidor, productor;

	Sem_init(&mutex, 0, 1);
	Sem_init(&w, 0, 1);

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];

	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{

		// Si se desea utilizar con respecto al tamaño del archivo
		// comentar esta sección del código y descomentar la siguiente.
		// Luego compile.

		sbuf_init(&buffer, BUFFER_SIZE);
		char bufrecibido[BUFFER_SIZE];

		// Si se desea utilizar de acuerdo al tamaño del archivo
		// descomentar esta sección del código y compilar.

		// int buffer_size;

		// buffer_size = fileStat.st_size;

		// sbuf_init(&buffer, buffer_size);
		// char bufrecibido[buffer_size];

		Pthread_create(&productor, NULL, escribir, filename);
		Pthread_create(&consumidor, NULL, leer, bufrecibido);
		Pthread_join(productor, NULL);
		Pthread_join(consumidor, NULL);
	}
	
	printf("Buffer Insertado: %s\n",buffer.buf);

	return 0;
}

void *escribir(void * argv){

	char *file;
	int count = 0;
	int fd;
	char c;
	file = (char *) argv;

	printf("Abriendo archivo %s...\n",file);
	fd = Open(file, O_RDONLY, 0);
		while(count < buffer.n){
			
			Read(fd,&c,1);
			P(&w);
			sbuf_insert(&buffer, c);
			V(&w);
			printf("Productor inserta: %c\n",c);
			count++;
			printf("Buffer Insertado: %s\n",buffer.buf);
			
			usleep(TIEMPO_MS * 1000);
		}			
		Close(fd);

	return NULL;
	
}

 void *leer(void *argv){

 	char *bufrecibido;
 	int count = 0;
 	char c;
 	bufrecibido = (char *)argv;

 	while(count < buffer.n){
 		usleep(TIEMPO_MS * 20 *1000);

		P(&mutex);		
		c = sbuf_remove(&buffer);
		V(&mutex);		
 		bufrecibido[count] = c;
 		printf("Consumidor lee: %c\n",c);
 		count++;
 		
 	}

 	printf("Leido Total: %s\n",bufrecibido);

 	return NULL;
 }



